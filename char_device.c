#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/ioctl.h>
#define KBUF_SIZE 64
#define DR_BASE 'k'
#define DR_RESET _IOW(DR_BASE, 1, unsigned long*)
#define DR_STOP _IOW(DR_BASE, 2, unsigned long*)
unsigned long value;
char kbuf[KBUF_SIZE]="Hello World \n";
//struct timer_list exp_timer;
//void do_smth(struct timer_list *timer);
static int dev_open (struct inode *inod, struct file *fil);
static int dev_release (struct inode *inod, struct file *fil);
static ssize_t dev_read (struct file *fil, char *buff, size_t len, loff_t *off);
static ssize_t dev_write (struct file *fil, const char *buff, size_t len, loff_t *off);
static long dev_ioctl (struct file *fil, unsigned int cmd, unsigned long arg); 
static struct file_operations fops =
{
	.owner = THIS_MODULE,
	.open = dev_open,
	.release = dev_release,
	.read = dev_read,
	.write = dev_write,
	.unlocked_ioctl = dev_ioctl,
};
/*void do_smth(struct timer_list *timer){
	printk(KERN_ALERT "Round !");
	mod_timer(timer, jiffies+2*HZ);
}
*/
static int __init dev_init(void){
	printk(KERN_INFO "Init \n");
	int t = register_chrdev(90, "char_device", &fops);
	if(t<0){
		printk(KERN_ALERT "registration failed ! \n");
	}
	else{
		printk(KERN_ALERT "registration success ! \n");
		//timer_setup(&exp_timer, do_smth, 0);
		//mod_timer(&exp_timer, jiffies+2*HZ);
	}
	return t;
}
static void __exit dev_exit(void){
	unregister_chrdev(90, "char_device");
	//del_timer(&exp_timer);
        printk(KERN_INFO "GoodBye \n");
}
static int dev_open (struct inode *inod, struct file *fil){
	printk(KERN_ALERT "Open \n");
	return 0;
}
static int dev_release (struct inode *inod, struct file *fil){
	printk(KERN_ALERT "Close \n");
	return 0;
}
static ssize_t dev_write (struct file *fil, const char *buff, size_t len, loff_t *off){
	int i, mylen;
	printk(KERN_ALERT "Write \n");
	if(len > KBUF_SIZE-1){
		mylen = KBUF_SIZE-1;
	}
	else{
		mylen = len;
	}
	for(i=0; i<mylen; i++){
		get_user(kbuf[i], buff+i);
	}
	for(i=mylen; i<KBUF_SIZE; i++){
		kbuf[i] = 0;
	}
	printk(KERN_ALERT "%s \n", kbuf);
	return len;
}
static ssize_t dev_read (struct file *fil, char *buff, size_t len, loff_t *off){
	ssize_t ret=0;
	printk(KERN_ALERT "Read \n");
	if(*off==0){
		if(copy_to_user(buff, kbuf, sizeof(kbuf))){
			ret = -EFAULT;
		}
		else{
			ret = sizeof(kbuf);
			*off = 1;
		}
	}
	return ret;
}

static long dev_ioctl (struct file *fil, unsigned int cmd, unsigned long arg){
	copy_from_user(&value, arg, sizeof(value));
	switch(cmd){
		case DR_RESET:
			printk(KERN_ALERT "reset %lu \n", value);
			break;
		case DR_STOP:
			printk(KERN_ALERT "stop %lu \n", value);
			break;
		default:
			printk(KERN_ALERT "unknow ioctl \n");
	}
	return 0;
}	
module_init (dev_init);
module_exit (dev_exit);
MODULE_AUTHOR("SheryarM");
MODULE_DESCRIPTION("This module permits to R/W message to a char device");
MODULE_LICENSE("GPL");
MODULE_VERSION("1.0.0");
