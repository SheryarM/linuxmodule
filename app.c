#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#define DR_BASE 'k'
#define DR_RESET _IOW(DR_BASE, 1, unsigned long*)
#define DR_STOP  _IOW(DR_BASE, 2, unsigned long*)

int main(void){
	int fd;
	unsigned long value = 128;
	
	fd = open("/dev/char_device", O_RDWR);
	if(fd < 0){
		printf("Error while opening device file ! \n");
		return -1;
	}
	
	ioctl(fd, DR_RESET, (unsigned long*)&value);
	
	close(fd);
	
	return 0;
}
